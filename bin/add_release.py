#!/usr/bin/python

import argparse
import os
import subprocess


def expand_version(ver):
    # Expand a version string to be of the form major.minor.patch
    spl = ver.split('.')
    assert(len(spl) < 3)
    for i in spl:
        try:
            int(i)
        except ValueError:
            raise ValueError('Version numbers must consist of numbers only, separated by a dot')

    return "{}.{}.{}".format(spl[0], spl[1] if len(spl) > 1 else 0, spl[2] if len(spl) > 2 else 0)


def get_arg(arg=None):
    # Define command line arguments for this script
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', help='The version number for the newly created release', required=True)
    parser.add_argument('-s', '--source', help='Path to the sources for the release (a directory that contains a all dune modules)', required=True)
    parser.add_argument('-w', '--which', nargs='*', help='The list of dune modules to build. If omitted and --core not set, build all available')
    parser.add_argument('-c', '--core', action='store_true', help='Whether this is a core module release, superseded by --which')
    parser.add_argument('-h', '--homepage', help='The root directory of the hugo project. If omitted, assumed to be .')
    parser.add_argument('-k', '--key', help='The GPG key to sign the release tarballs with')
    v = vars(parser.parse_args())

    # Apply defaults
    if len(which) == 0 and v['core']:
        v['which'] = ['dune-common', 'dune-istl', 'dune-geometry', 'dune-grid', 'dune-localfunctions']

    if len(which) == 0:
        raise ValueError("No modules selected for release! Check command line arguments")

    v['version'] = expand_version(v['version'])

    if arg is None:
        return v
    else:
        return v[arg]


def generate_tarball(module, compression_flag=''):
    compression_ext = {'': '', 'z': '.gz'}[compression_flag]
    sp = os.path.join(get_arg['source'], module)
    assert(os.path.isdir(sp))
    ap = os.path.join(get_arg['homepage'], 'static', 'download', get_arg['version'])
    os.makedirs(ap)
    ap = os.path.join(ap, '{}-{}.tar{}'.format(module, get_arg['version'], compression_ext))
    ret = subprocess.call('tar xvf{} {} {}'.format(compression_flag, ap, sp).split())
    assert(ret == 0)
    ret = subprocess.call('gpg --detach-sign --armor --default-key {} {}'.format(get_arg['key'], ap).split())
    assert(ret == 0)


def package_release(module):
    generate_tarball(module, 'z')


def write_hugo_content():
    # First, create a new content page
    ret = subprocess.call('hugo new releases/{}.md'.format(get_arg['version']))
    mdfile = os.path.join(get_arg['homepage'], 'content', 'releases', '{}.md'.format(get_arg['version']))

    # Now collect a dictionary of information to provide to the generation process
    data = get_arg()
    data['majorversion'] = data['version'].split('.')[0]
    data['minorversion'] = data['version'].split('.')[1]
    data['patchversion'] = data['version'].split('.')[2]

    # And replace the placeholders
    lines = []
    f = open(mdfile, 'r')
    for line in f:
        lines.append(line.format(data))
    f.close()
    f = open(mdfile, 'w'):
    for line in lines:
        f.write(line)

    print("Hugo content written to {}. Add any information about this release that you might want to share to that file".format(mdfile))


if __name__ == '__main__':
    # Trigger the actual creation of the release:
    for module in get_arg['which']:
        package_release(module)

    # And write the hugo content for this release:
    write_hugo_content()
