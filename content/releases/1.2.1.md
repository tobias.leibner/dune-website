+++
date = "2009-07-29T16:03:35+01:00"
major_version = 1
minor_version = 2
modules = ["dune-common", "dune-istl", "dune-grid", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 1
title = "Dune 1.2.1"
version = "1.2.1"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 1.2.1 - Release Notes

**Warning:** Many things have been marked deprecated. However note that class deprecation is buggy in all versions of gcc prior to 4.3.

## dune-common

*   Corrected version dependencies such that pkg-config can parse them.
*   Fixed compatibility issue of PoolAllocator and gcc-4.3 (FS #543).
*   Added copy constructor for `FieldVector` (to avoid warnings on an unused variable for size 0).

## dune-grid

*   Fixed method `conforming` on `ALUGrid` intersection iterator.
*   Corrected adaptation interfaces for `ALUGrid`, `OneDGrid` and `UGGrid`. A test for these features was also added.
*   The method `name` of `ALUConformGrid` now returns `"ALUConformGrid"`.
*   Fixed a compatiblity issue between `AlbertaGrid` and MPICH.
*   Ported several fixes and enhancements to the generic geometries for a smoother transition.
*   Fixed callback adaptation for periodic `AlbertaGrid` and improved the documentation.
*   Fixed a failing assertion in `ALU3dGrid` when calling `ilevelbegin`.

## dune-istl

*   AMG: Set overlap to zero for coarse levels to avoid degradation of convergence when running parallel.
*   Some solvers incorrectly stated that they converged.
*   Added random access iterators to ISTL base arrays.
*   AMG: Fixed an incorrect type in `IndicesCoarsener`.
*   Make sure that some FieldMatrices are initialized.
*   Fixed communication for containers with a variable size of data items per index
*   AMG: Interpret positive off-diagonal values as weak couplings as the error might oscillate (fixes convergence for anistropic problems with trilinear finite elements).
*   ILU preconditioners: Allow const matrices as template parameters. (Caused a compile time error before)
