+++
major_version = 2
minor_version = 1
modules = ["dune-common", "dune-istl", "dune-grid", "dune-localfunctions", "dune-grid-howto", "dune-grid-dev-howto"]
patch_version = 1
title = "Dune 2.1.1"
version = "2.1.1"
signed = 0
[menu]
  [menu.main]
    parent = "releases"
    weight = -1

+++

# DUNE 2.1.1 - Release Notes

*   All modules compile with g++ 4.6, improved Clang support.

## buildsystem

*   dunecontrol improves handling of locally installed modules and modules from SVN.

## dune-common

*   Fixed nullptr with g++ 4.6.
*   Fixed memory leaks in nullptr and className.

## dune-grid

### Deprecated or removed features

*   The methods <tt>GmshReader::read(GridType &grid, const std::string &fileName, *)</tt> which take a grid object as an argument are deprecated in Dune 2.1.1\. These methods are UGGrid-specific quirks.

## dune-istl

## dune-localfunctions

*   Add missing parts of the 3d specialization of <tt>RefinedP0LocalFiniteElement</tt>. (Thanks to Uli Sack for the patch)

## dune-grid-howto

## dune-grid-dev-howto
