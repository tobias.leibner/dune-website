+++
title = "SPE10 benchmark"
content = "carousel"
image = ["/img/dumux.png"]
+++

Simulation of a five-spot injection scenario with a strongly
heterogeneous domain (SPE10 benchmark) using an IMPES formulation on
an adaptively refined grid. For details see
[dumux](http://www.dumux.org/).

<!--more-->
