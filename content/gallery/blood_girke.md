+++
title = "Blood flow"
content = "carousel"
image = ["/img/blood_girke.png"]
+++

Blood flow through a
narrowed carotid artery. Simulation of atherosklerosis.
(Simulation by Stefan Wierling)
