+++
title = "Noisy image recovery using phase field models"
content = "carousel"
image = ["/img/leopardtodune.gif"]
+++

Binary image recovery using a phase field approximation on a dynamic grid.
[BDE](https://doi.org/10.1007/978-3-319-08025-3_2).

<!--more-->
