+++
title = "Dune Developer Meeting 2018 in Stuttgart"
+++

Date
----

The developer meeting is held on November 7th and 8th, 2018 in Stuttgart.

The meeting is co-located with the [Dune User Meeting 2018](https://dune-project.org/community/meetings/2018-11-usermeeting/).

Participants
-------------

- Andreas Dedner
- Christoph Grüninger
- Christian Engwer
- Robert Klöfkorn
- Oliver Sander
- Jorrit Fahlke
- Claus Heine
- Marcel Koch
- Nils-Arne Dreier
- Dominic Kempf
- Bernd Flemisch
- Birane Kane
- Linus Seelinger
- Steffen Müthing
- Stephan Hilb
- Mirco Altenberg
- Martin Alkämper
- Samuel Burbulla
- Markus Blatt
- Timo Koch

Proposed topics
---------------

Please use the edit function to add your topics or write an email to [the organizers](https://www.ians.uni-stuttgart.de/institute/news/events/Dune-User-and-Developer-Meeting-2018/).


## General

* RK: Stability of the gitlab infrastructure. How can we help to improve it? 
* CGü: I'd like to see an hour of cleaning up. Together we scan (one or two of)
  - merge requests
  - bugs
  - the remaining open FlySpray tasks
  - Git branches
  
  and decide which can be closed / deleted right away and which to keep.

## dune-common

## dune-geometry

## dune-grid

* OS: ALUGrid has been implementing some improvements to the distributed grids
  interface.  Can we make some of the part of the official interface?  
  - RK: That would be nice and should not be much work or disturbance for other grid implementations.
  - CGü: Do you have concret suggestions what to include?

* RK: Should we add a grid implementation providing polyhedral grids?
  - CE: I'm in favor of adding a wide range of different grids to the core to improve the first shot user experience.

## dune-localfunctions

## dune-istl

* RK: What is needed to make dune-istl a more self contained LA solver package?
* CE: I'd like to finish our long standing effort of fully dynamic solver interfaces and a solver factory. There are a few smaller questions where I'd like to have some feedback from the core developers.
* OS: The fact that FieldVector<X,1> and FieldMatrix<X,1,1> get special treatment
      is the source of various inconsistencies and general unhappiness.  Can we review that decision?
      There are two competing proposals:
  - Make nesting recursion end at regular number types, rather than at FieldVector/FieldMatrix.
    For example, allow `BCRSMatrix<double>` and make it do what people would expect.
  - Introduce a dedicated new type `Field<T>` that should wrap number types T and that
    should be used to end recursions.
    This would mean that `BCRSMatrix<FieldMatrix<double,1,1> >` should be replaced by
    `BCRSMatrix<Field<double> >`.

  The interesting question: Can we do that in a backward-compatible way?     

## Discretization modules (FEM, PDELab, ...)

* CE: can we agree on a new common discrete function interface based on dune-functions? Current state is
  - dune-functions defines interfaces for functions and global bases.
  - a couple of people are now working directly with dune-functions, without any other discretization module.
  - PDELab supports the new function interface together with "old-style" PDELab gridfunctionspaces
  - FUFEM is now mostly based on dune-functions
* CGrä: Some comments on CE's proposal above:
  - Notice that dune-functions `localFunction(f)` interface is already supported in the `VTKWriter`
  - When discussing the proposal please mention the subtleties of
    `localFunction(derivative(f))` vs `derivative(localFunction(f))` to avoid
    later surprises.

## python support

* CE: How do we envision future code development regarding python?


# Protocol

## User Meeting Input

- Parallel grid construction  
  - would be possible with a discussed extension of the grid interface, boundary segments will not work
- Data I/O (HDF5 / XDMF)  
  - will be a working group on second day of Dune dev meeting
- Binary operators for dense linear algebra  
  - does currently not exist because of past performance concerns
  - Oliver will provide an example implementation, Oliver thinks about which operators needs to be implemented
  - needs benchmarking (to prevent performance penalty), will be done by Jö
- Dune packaging for spack https://github.com/spack/spack
  - users seem not to be interested
  - interesting concept, might be enough to get get Dominic working on it
- News entries on homepage for applications to build application showcases
  - idea: new section latest publications, last three on start page with link "more" to complete list; new list ist above or below nes entries (Andreas and Robert will investigate this)
- Periodic Boundaries
  - Yasp implements periodicity as ghost elements and requires parallel execution
  - ALUGrid and SPGrid implements that differently by identifing entities
  - Yasp does not correctly answer `boundary` and `neightbor` on periodic boundary
  - Alu and SPGrid does not transform  outsideElements at periodic boundary
  - Timo and Dominic will complete the documentation


## Agenda

Suggestion by Oliver: working groups

- Documentation / website
- Build system: WG (Steffen, Markus, Dominic, Jö, Linus)
- Discuss new Dune paper: Thursday
- Development Process (handling of MRs etc.)
- Bindings for other programming languages
- Infrastructure
- MPI rework
- New way of handling scalar LA entities (`FieldVector<T,1>`, `FieldMatrix<T,1,1>`) (Jö, Oliver, Claus)
- Minimal versions of toolchain components
- Dynamic selection of solver components in ISTL
- I/O: WG (Steffen, Timo, Markus)
- dune-localfunctions interface: WG (Oliver, Andreas, Christian)
- twists and grid topology: WG (Marcel, Christian, Claus, Robert)
  - First approach: Avoiding twis problem by creating edge consistent grids -> module from Marcel.
  - Make functionality usable without dune?
  - Grids need flags: will consistency be preserved during loading/refinement/load balancing
  - Different ways of refinement (conforming/nonconforming) -> document behaviour
  - Second approach: How to handle non edge consistent grids
  - Load local dofs, for every subentity transform according to inconsistency, revert transformation before writing into global data structure
  - Transformation not always permutation: Momentums, Raviart Thomas, ...
  - Local Finite Element should provide object that can provide transformation for subentity dofs
  - Special implementation if transformation only permutation or only integers in matrix
  - Local Finite Element should provide information about pullback
- dune-functions as a foundation for discretization modules
- parallel grid interface including parallel grid construction: WG (Robert, Markus, Linus, Oliver, Andreas, Christian, Nils)
  - communicate on Grid will return a future object, should act like std::future< void >/MPIFuture?
  - interface for repartition should be discussed based on AluGrid/UGGrid implementations
  - conclude that existing entity iterators are sufficient to compute border entity ranges
  - overlapSize and ghostSize?
  - ghost elements in ALUGrid, fix documentation
- provide constexpr functions to gain DoF for entities etc. (Christian and Steffen extract use-cases from PDELab)

## Build System

Input

- Oliver: Alias `dunecontrol cmake` for `dunecontrol configure`
- Build without dunecontrol
  - "Master CMake project" with subprojects for each module
  - conan.io package manager

Result from working group

- Steffen has an aged proposal
- if not too much work for downstream modules, adapt his approach
- CMake features tests (e.g. searching for MPI) are only performed once
- no longer call to module-specific test from cmake/modules/Dune<Module>Macros.cmake, they should be revised and integrated in the top-level CMakeLists.txt
- libraries are re-exposed with CMake target mechanism
- every dune module has a CMake target, maybe header only, pulling in all module dependency to external librarys (including dependencies between Dune modules)
- down-stream modules, which do not have CMake feature tests, should work without breaking change (some calls should be removed or changed, but for a smooth transistion it should be possible to provide according methods for one release)
- config.h renamed per module, e.g., dune-common-config.h
- no longer support static and dynamic libraries
- plan: Steffen will tidy up his prototype, further development in branches, we will discuss before merging (hopefully without breaking too much)

## Website

- Add a rotating image gallery (JavaScript anyone?)
- (Issue #54) Grid documentation: Make it easier to find list of [grid features](https://www.dune-project.org/about/gridmanager-features/)


## Miscellaneous

- What does `Entity::regular()` do?
  - Nobody knows whether it's needed
  - Remove it
- `GeometryType::GeometryType(int dim)` and `GeometryType::GeometryType(unsigned int dim)`
  - only work for vertices and lines
  - remove
- Improve deprecation macros to encode version
- `MPIHelper::instance()` why not const ref?


## Release Procedure

- Time-Based releases
  - Requires automatic testing

## I/O

- Do we want to support generic backup / restore using `GridFactory` or something similar?
- Use Python bridge for backup / restore
- Improve backup / restore documentation

## dune-localfunctions interface

Result from working group
- dynamic interface has a lot of flaws and few users
- run-time interface uses type erasure
- infrastructure for type erasure exists in parts in dune-functions
- create merge request for dune-localfunctions
- deprecate dynamic interface

- issues with interpolate, no consensus

- provide a way to get points of degree of freedom for local subentity
- currently discretization modules remap local keys anyway, but should be kept

## Future of GitLab infrastructure
- People taking care of GitLab might leave academia in summer 2019
- Migrate to GitLab Cloud, downsides are Git commit hooks, less control
- current server issues are resolved

## extend Dune grid interface
- methods from ALUGrid that would be beneficial for all
- parallel grid creation
  - UG should implement them, too (Oliver Sander thinks it could be done)
  - two methods `insertVertex` and `insertProcessBorder`
  - change accepted, waiting for merge request

- rebalance grid partition
  - method `repartition`
  - just discussion, no decision

## unify grid readers
- dune-grid issue 23, dune-grid merge request 90
- `readImp` should have a second form accepting an input stream
- return parameters for element/boundary data should be unified
- Maybe use an object instead of static methods, maybe this object should behave more like a grid factory

## output of matrices and vectors
- need a simple interface to output matrices and vertices
- Linus will propose a merge request

## asynchron MPI
- dune-common merge request 526
- introduce asynchronous MPI features like future
- data abstraction for communicate and serialization
- comments in MR

## result of I/O group

### General formats?
- DGF (simple data), Gmsh (simple data), ...
- VTK (visual + simple data)
- XDMF/HDF5 (visual + complex data)

### Check-pointing
- use backup-restore facility (does it get a stream?)
- check-pointing is specialized for each user/discretization module
- exadune has a prototype for serializing a DG simulation in HDF5
- dune-pdelab has an implementation, dune-fem has an implementation for restart
- generalized interface in dune is generally wishful, but should we invest the time? and who?
- Checkpointing: dune-fem/dumux/pdelab have their own solutions. Should we invest work to clean those up and have something in the core modules?

### HDF5
- dune-hdf5
- put all implementations in an experimental folder
- improve HDF CMake check
- leightweight HDF5 C++ Wrapper
- HDF for dumping grid and data for visualization
- XDMFWriter optionally gets HDF5Writer from outside

### Vtk

- VtkWriter double precision -> try runtime penalty for if/else for
every field (runtime) (-> Timo)
  - VtkWriter gets outputFieldType for grid (default to float)
  - every function gets field type in FieldInfo (default to float)
  - otherwise template argument for Writer and every field
- deprecate old VtkFunction
- set counter bei sequence writer (-> Bernd)
  - setting the vector of time steps from outside might be what this actually means
  - should be suitable for restart

### Reader

- Reader should have a corresponding writer and the other way around with hopefully high number of matching features
- Sequential reader interface, reads on process 0, distributes (-> Dumux team)
  - Supports different grid managers (depending on what the format gives us, e.g. structured/unstructured)
  - gives out unique_ptr<Grid>
  - gives out data (codim-dim codim-0 boundaries) as std::vector<std::vector<FieldType>>
  - provides mappings (in case of resorting), grid factory?
  - can do load-balacing of data
