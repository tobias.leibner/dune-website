+++
# The name of the module.
module = "dune-xt (dune eXTensions)"

#For page title in browser
title = "dune-xt"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = ["extension"]

# List of modules that this module requires
requires = ["dune-common", "dune-pybindxi", "dune-testtools", "dune-istl"]

# A string with maintainers to be shown in short description, if present.
maintainers = "[René Fritze](mailto:rene.fritze@wwu.de), [Tim Keil](mailto:tim.keil@wwu.de), [Tobias Leibner](mailto:tobias.leibner@wwu.de), [Felix Schindler](mailto:felix.schindler@wwu.de)"

# Main Git repository, uncomment if present
git = "https://github.com/dune-community/dune-xt-super"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "Adds convenience features and enables programming of generic algorithms."
+++

# What are the dune-xt modules?

The dune-xt (DUNE eXTensions) project aims to provide an extra layer of usability and convenience to existing DUNE
modules and at the same time to enable programming of generic algorithms, in particular in the context of
discretization frameworks. It consists of the five modules
[dune-xt-common](https://github.com/dune-community/dune-xt-common),
[dune-xt-data](https://github.com/dune-community/dune-xt-data),
[dune-xt-grid](https://github.com/dune-community/dune-xt-grid),
[dune-xt-la](https://github.com/dune-community/dune-xt-la) and
[dune-xt-functions](https://github.com/dune-community/dune-xt-functions).

The dune-xt-common module provides:

- Vector and Matrix abstractions that allow programming of generic algorithms
- String conversion utilities for vectors and matrices
- FloatCmp for vectors and matrices
- Extensions of FieldVector and FieldMatrix that add features like additional algebraic operators
- Utilities to simplify handling of parametric functions and equations
- Several other convenience classes

The dune-xt-data module serves a storage for data files that are needed in other dune-xt modules. In particular, it contains several quadratures:

- Gauss-Lobatto quadratures up to order 197
- Fekete quadratures (on triangles) up to order 18
- Quadratures on the unit sphere: Lebedev quadratures up to order 131, octant-wise quadratures up to order 70 and product quadratures (in spherical coordinates).

In addition, the dune-xt-data module contains code to numerically calculate matrix exponentials.

The dune-xt-grid module provides the following features:

- The GridWalker which registers an arbitrary number of functors and applies them all in a single (thread-parallel) walk over the grid.
- GridProviders to allow generic creation of different grids
- Utilities for simple handling of boundary conditions
- A PeriodicGridView class that turns every GridView into a periodic gridview at the specified boundaries
- An EntitySearch which can find entities by domain coordinates (e.g. for projections and prolongations on adaptive grids)

The dune-xt-la module contains:

- Thread-safe linear algebra containers (vectors and matrices)
- Runtime selectable linear solvers
- Generic linear algebra algorithms (e.g. Cholesky and QR decomposition, Eigensolvers) that work with any vectors and matrices for which a VectorAbstraction or MatrixAbstraction, respectively, exist, in particular with dune-common's matrix and vector classes.

The dune-xt-functions module provides functions that can be evaluated in global coordinates and localizable function classes that can be bound to entities and evaluated in local coordinates.


# How to use the dune-xt modules
We provide a git repository containing the dune-xt modules and all required dependencies. You can find this super module and installation instructions [here](https://github.com/dune-community/dune-xt-super). If you want to use our discretization module dune-gdt, you should use the [dune-gdt-super](https://github.com/dune-community/dune-gdt-super) repository instead.

You can also use (some of) the dune-xt modules in your existing DUNE setup, just put it as a requirement or suggestion into the dune.module file of your own module and configure/build it through dunecontrol (see the documentation of dune-common for details). Of course you also have to add the dependencies listed above (dune-pybindxi is available [here](https://github.com/dune-community/dune-pybindxi)).
