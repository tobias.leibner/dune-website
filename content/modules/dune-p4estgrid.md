+++
git = "https://gitlab.dune-project.org/robert.kloefkorn/dune-p4estgrid"
group = ["grid"]
maintainers = "Robert Klöfkorn, Martin Nolte"
module = "dune-p4estgrid"
short = "A grid implementation using the [p4est](http://p4est.org/) library"
title = "dune-p4estgrid"
+++
