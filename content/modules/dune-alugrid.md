+++
module = "dune-alugrid"
group = ["grid"]
requires = ["dune-common", "dune-geometry", "dune-grid"]
suggests = ["dune-python"]
maintainers = "Martin Alkämper, Andreas Dedner, Robert Klöfkorn, Martin Nolte"
git = "https://gitlab.dune-project.org/extensions/dune-alugrid"
short = "ALUGrid is an adaptive, loadbalancing, unstructured implementation of the DUNE grid interface supporting either simplices or cubes."
+++

The dune-alugrid module is the successor to the ALUGrid implementation previously
provided in dune-grid.
It provides an adaptive, loadbalancing, and unstructured implementation of the
DUNE grid interface in two or three space dimensions supporting either simiex
or cube elements.

New features and improvements include

  * Conforming refinement for the 3D simplex grid
  * All grids are now parallel (i.e., 2D and 3D)
  * Internal load balancing based on space filling curves
    making DUNE-ALUGrid self contained also in parallel
  * Bindings for fully parallel partitioners using [Zoltan](http://www.cs.sandia.gov/Zoltan/)
  * Complete user control of the load balancing
  * Improved memory footprint

The old ALUGrid version is deprecated and not supported anymore.
We have removed the special grid types e.g. ALUConformGrid, ALUSimplexGrid, and ALUCubeGrid.
Instead the type of the grid is always of the form
Dune::ALUGrid< dimgrid, dimworld, eltype, refinetype, communicator > (where communicator has a default value).
The values for eltype are cube,simplex and for refinetype the values are conforming, nonconforming defined in the DUNE namespace.
The GRIDTYPE defines can still be used as before.

A detailed description of all the new features and some more
details concerning the inner workings of DUNE-ALUGrid can be found
in the paper
[Alkämper, Dedner, Klöfkorn, Nolte. The DUNE-ALUGrid Module, Archive of Numerical Software 4(1), 2016](http://journals.ub.uni-heidelberg.de/index.php/ans/article/view/23252).

**This is the paper we would ask everyone to cite when using DUNE-ALUGrid.**


License
-------

The DUNE-ALUGrid library, headers and test programs are free open-source software,
licensed under version 2 or later of the GNU General Public License.
