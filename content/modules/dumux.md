+++
# The name of the module.
module = "dumux"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = ["user"]

# List of modules that this module requires
requires = ["dune-common", "dune-grid", "dune-localfunctions", "dune-istl", "dune-geometry"]

# List of modules that this module suggests
suggests = ["dune-alugrid", "dune-foamgrid", "dune-uggrid", "opm-grid", "dune-subgrid", "dune-spgrid"]

# A string with maintainers to be shown in short description, if present.
maintainers = "[The DuMux team](/modules/dumux#Team)"
title = "DuMux"

# Main Git repository, uncomment if present
git = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "DuMux is a module for simulation of multi-phase multi-component flow transport in porous media"

# Doxygen documentation: Please specify the following keys to automatically build
# a doxygen documentation for this module. Note, that specifying the git key is
# necessary in this case. All of the keys should be lists of the same length.
# Each entry of the list specifies the parameter for a separate piece of documentation.
# You can use this feature to generate documentation for several branches.
#
# Specify the url, where to build the doxygen documentation
# doxygen_url = ["/doxygen/dune-composites/release-2.5"]
# Specify the branch from which to build, omit to build from master
# doxygen_branch = ["releases/2.5"]
# Specify to build a a joint documentation from the following list of modules,
# omit, to build a doxygen documentation only for this module. This list will
# be used for all documentations, no list of lists necessary...
# doxygen_modules = []
# Please specify the name of the doxygen documentation, that will be shown on the main page.
# doxygen_name = ["dune-composites"]
# doxygen_version = ["2.5.1"]
+++

DuMux, DUNE for Multi-{Phase, Component, Scale, Physics, ...} flow and transport in porous media, is a free and open-source simulator for flow and transport processes in porous media. Its main intention is to provide a sustainable and consistent framework for the implementation and application of porous media model concepts and constitutive relations. It has been successfully applied to CO2 storage scenarios, environmental remediation problems, transport of therapeutic agents through biological tissue, flow in fractured porous media, simulation of root-soil interaction, and subsurface-atmosphere coupling.

More information on DuMux can be found on the [DuMux website](http://www.dumux.org/).

### Class Documentation

The current class documentation and the DuMux handbook can be found at:

http://www.dumux.org/documentation.php

### <a name="Team"></a> Team
* [Bernd Flemisch](http://www.hydrosys.uni-stuttgart.de/institut/mitarbeiter/person.php?name=1143)
* [Other developers](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/graphs/master)
