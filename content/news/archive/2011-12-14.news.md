+++
date = "2011-12-14"
title = "dune-common-2.1.1 maintenance release"
+++

We have just released dune-common-2.1.1. The new version is a maintenance release. It does not offer any new features or interface changes, but fixes various important bugs.
