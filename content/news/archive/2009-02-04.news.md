+++
date = "2009-02-04"
title = "Release Branch for Dune 1.2 created"
+++

We have created the release branch for the upcoming Dune 1.2 release. The release will contain the modules `dune-common`, `dune-istl`, `dune-grid`, `dune-grid-howto`, and `dune-grid-dev-howto`. These modules can be downloaded using subversion

    svn checkout https://svn.dune-project.org/svn/dune-common/releases/1.2 dune-common

    svn checkout https://svn.dune-project.org/svn/dune-common/tags/1.2 dune-common

Martin Nolte and Oliver Sander have been named release managers. We intend to concentrate on stability and documentation for a few weeks and have the actual release by the beginning of next month.
