+++
date = "2013-12-17"
title = "UG patch 11 released"
+++

We have released a new version of the [UG patch file]($(ROOT)/external_libraries/install_ug.html). The new patch level number is *11*. This is the first release known to work seamlessly with the new CMake build infrastructure of the trunk version and planned 2.3 release of the core modules.
