+++
date = "2014-01-09"
title = "Dune 2.3beta1 Released"
+++

A first set of release candidates for the upcoming 2.3 release is now available. This includes all core modules. You can download the tarballs from our download page. The modules are also available from [Debian Experimental](http://packages.debian.org/search?keywords=libdune&searchon=names&suite=experimental&section=all). Please go and test, and report the problems that you encounter.
