+++
date = "2009-12-01"
title = "New Patch Set of UG Available"
+++

An updated patch set for UG has just been released. You need these patches if you want to use UGGrid from the Dune svn development branch. They contain various bug fixes and improvements, mainly related to

-   using very large coarse grids
-   making the parallel features of UG available in Dune.

The new patches are available for download with the [installation instructions](http://www.dune-project.org/external_libraries/install_ug.html).
