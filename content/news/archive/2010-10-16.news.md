+++
date = "2010-10-16"
title = "UG-3.9.1-patch2 Patch File Released"
+++

A new patch file for the UG grid manager has been released. It contains mainly build system fixes and improvements, but should also speed up the creation of very large grids. See the [changelog]($(ROOT)/external_libraries/install_ug.html) for details. From this release on we will start to use proper versions for patched UGs (previously they were all called UG-3.9.1).

Users of dune-grid revision 7008 and lower may choose to update their UG installation, but this is not required. The UG-3.9.1-patch2 patch set is mandatory for later revisions.
