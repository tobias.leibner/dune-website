+++
date = "2009-10-26"
title = "DUNE-FEM 1.0.0 Released"
+++

The [Dune-Fem module](http://dune.mathematik.uni-freiburg.de/) is based on the Dune-Grid interface library, extending the grid interface by a number of higher order discretization algorithms for solving non-linear systems of partial differential equations. This includes for example the automatic handling of the degrees of freedom (dof) on locally adapted and parallel grids - including dynamic load balancing. For more details, have a look at the [Dune-Fem page](http://dune.mathematik.uni-freiburg.de/).
