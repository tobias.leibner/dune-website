+++
date = "2007-05-09"
title = "DUNE Summer School in Freiburg"
+++

From 6th to 9th of August 2007 the DUNE Summer School "Discretization of Evolution Equations using DUNE-Fem" will take place in Freiburg. See the [Summer School's web page](http://www.mathematik.uni-freiburg.de/IAM/Teaching/ubungen/dune/eindex.html) for detailed information.
