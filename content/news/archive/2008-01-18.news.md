+++
date = "2008-01-18"
title = "DUNE short course (March 13-14, 2008)"
+++

The DUNE group in Stuttgart is pleased to a announce its first DUNE Course. It will take place March 13-14, 2008 at IPVS, University Stuttgart.

By participating in this course scientists have the opportunity to get a hands-on introduction to the DUNE framework. Main focus is to give a detailed introduction to the DUNE core modules: the grid interface including IO methods with its numerous grid implementations and the iterative solver module ISTL. In the exercises a hyperbolic sample problem will be implemented (convection equation using Discontinuous-Galerkin discretisation with constant and linear shape functions).

For people not already familiar with generic programming, static polymorphism, templates and the STL, there is the possibility to get insight in these areas by participating in the "Advanced C++" course held on March 12, 2008.

For further information see the [course homepage](http://ipvs.informatik.uni-stuttgart.de/sgs/dune-course/).
