+++
date = "2012-07-26"
title = "Dune-Fem 1.3.0 Released"
+++

The Dune-Fem developers are pleased to announce the release of version 1.3.0 of the Dune-Fem module. The release brings you some new features and eliminates a number of known issues.

Dune-Fem is a discretization module which provides interfaces and implementations for the numerical solution of Partial Differential Equations using Finite Element Methods, Finite Volume Methods, and Discontinuous Galerkin Methods.

For further information and downloads see the [Dune-Fem homepage](http://dune.mathematik.uni-freiburg.de/).
