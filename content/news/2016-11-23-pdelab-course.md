+++
date="2016-11-23"
title = "DUNE/PDELab Course at Heidelberg University (March 6 - 10, 2017)"
+++

The Interdisciplinary Center for Scientific Computing at Heidelberg University will host its annual
DUNE and PDELab course on March 6-10, 2017.

This one week course provides an introduction to the most important DUNE modules and especially to
DUNE-PDELab. At the end the attendees will have a solid knowledge of the simulation workflow from
mesh generation and implementation of finite element and finite volume methods to visualization of the
results. Topics covered are the solution of stationary and time-dependent problems, as well as local
adaptivity, the use of parallel computers and the solution of non-linear PDEs and systems of PDEs.

#### Dates

March 6, 2017 - March 10, 2017

#### Registration Deadline

Friday, February 15, 2017

For further information, see the [course homepage](https://conan.iwr.uni-heidelberg.de/events/dune-course_2017/).
