+++
date = "2017-06-12"
title = "DUNE Mailing list addresses changed"
+++

The <span style="font-variant: small-caps">Dune</span> mailing lists have
moved.  Previously they were using addresses in the `@dune-project.org`
domain; this has been changed to `@lists.dune-project.org`.  Please update
your address books, and, if applicable, your mail filters.  The web interface
for administrative tasks and list archives continues to be available at
<https://lists.dune-project.org>.

In addition, lists that were no longer needed have been removed.

The mailing lists are now hosted by the
[group of Christian Engwer](http://wwwmath.uni-muenster.de/num/Arbeitsgruppen/ag_engwer/)
at the [WWU Münster](http://www.uni-muenster.de).  We thank the
[group of Peter Bastian](https://conan.iwr.uni-heidelberg.de) for hosting the
mailing lists for more than 15 years!
