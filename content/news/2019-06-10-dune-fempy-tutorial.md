+++
date = "2019-06-10"
title = "Tutorial for the dune-fem python bindings"
tags = [ "publications", "python", "core", "dune-fem" ]
+++

A detailed description of the [dune-fem python bindings] is now available.
It contains an introduction to the main concepts and to
the core bindings. Many individual Python script / Jupyter notebooks are available
for download showing how to solve a wide range of complex
problems. These provide a good starting point for new users.
Extensions of DUNE-FEM (e.g. solvers based on a
wide range of [Discontinuous Galerkin] and [Virtual Element] methods)
are also showcased.

[dune-fem python bindings]: /sphinx/content/sphinx/dune-fem
[Discontinuous Galerkin]: https://gitlab.dune-project.org/dune-fem/dune-fem-dg
[Virtual Element]: https://gitlab.dune-project.org/dune-fem/dune-vem
