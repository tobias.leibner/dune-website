+++
date = "2018-04-04"
title = "dune-uggrid 2.6.0 released"
tags = [ "releases" ]
+++

A new version of _dune-uggrid_, the grid manager from the UG finite
element software, has been released.  The new _dune-uggrid_ 2.6.0
release is compatible with DUNE 2.6 and now allows transfer element
data in addition to vertex data during load balancing.

The module can be downloaded from the [dune-uggrid page].

  [dune-uggrid page]: https://www.dune-project.org/modules/dune-uggrid
