+++
date = "2016-09-21"
title = "Preparations for DUNE 2.5 begin"
+++

A year after the release of DUNE 2.4, preparations for a new release, DUNE 2.5, have begun.
Our current rough plan is to create a release branch October, 17th, plan for a first release candidate November, 7th.
If required, followed by a second release candidate November, 21st.
The final release would then take place either November, 21st or December, 5th (in case of two release candidates).

The release will be managed by [Ansgar Burchardt](mailto:Ansgar.Burchardt@tu-dresden.de) and [Oliver Sander](mailto:Oliver.Sander@tu-dresden.de).
