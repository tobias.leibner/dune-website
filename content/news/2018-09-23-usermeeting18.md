+++
date = "2018-09-23T22:14:00+01:00"
title = "Invitation to the Dune User and Developer Meeting 2018"
tags = [ "events", "core", "users" ]
+++

We are going to organize a user meeting in Stuttgart, at the
conference hotel [campus.guest](https://www.campus-guest.de/en/)
at the Vaihingen campus of the University of
Stuttgart. The Dune User Meeting will start on the 5th of November and will continue on
the 6th. The Dune developer meeting will be held at the same venue
right after the user meeting ending on the 8th.

Details are available on the
[workshop webpage][webpage].

[webpage]: https://www.ians.uni-stuttgart.de/institute/news/events/dune-user-and-developer-meeting-2018
